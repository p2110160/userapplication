import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../user.service';
import { User } from '../models/userData';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit{

  user:User = { id: 0, name: '', occupation: '', email: '', bio: '' };

  constructor(private route: ActivatedRoute,
    private userService: UserService,
    private activatedRoute: ActivatedRoute){}

  ngOnInit(){
    this.route.params.subscribe(params => {
      const userId = parseInt(this.activatedRoute.snapshot.params['id'], 10);
      
      if (!isNaN(userId)) {    
        this.userService.getUserDetails(userId).subscribe(
          userData => {
            this.user = userData || { id: 0, name: '', occupation: '', email: '', bio: '' };
          },
          error => {
            console.error('Une erreur s\'est produite lors de la récupération des détails de l\'utilisateur :', error);
          }
        );
      } else {
        console.error('ID de l\'utilisateur non valide.');
      }
    });

  }
}
