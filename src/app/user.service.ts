import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from './models/userData'; 

@Injectable({
  providedIn: 'root'
})
export class UserService {
  
  private apiUrl='https://6580b4633dfdd1b11c41f9fa.mockapi.io/users';

  constructor(private http: HttpClient) { }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(`${this.apiUrl}`);
  }
  
  getUserDetails(userId: number): Observable<User> {
    const url = `${this.apiUrl}/${userId}`;
    return this.http.get<User>(url);
  }

  updateUser(user: User): Observable<User> {
    const url = `${this.apiUrl}/${user.id}`;
    return this.http.put<User>(url, user);
  }

  deleteUser(userId: number): Observable<User>{
    const url = `${this.apiUrl}/${userId}`;
    return this.http.delete<User>(url);
  }

  addUser(user:User): Observable<User>{
    const url = `${this.apiUrl}`;
    return this.http.post<User>(url, user);
  }

}
