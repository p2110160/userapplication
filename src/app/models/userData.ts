// src/app/mon-modele/mon-modele.ts
export interface User {
    name: String;
    occupation: String;
    email: String;
    bio: String;
    id: number;
  }
  