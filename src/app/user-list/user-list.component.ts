import { Component,OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
// src/app/mon-composant/mon-composant.component.ts
import { User } from '../models/userData';
import { Router } from '@angular/router';
import { UserService } from '../user.service';



@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent {

  constructor(private userService: UserService,private router: Router){

  }

  UserData: User[] = [];

  dataSource = new MatTableDataSource<User>(this.UserData);
  columnsToDisplay: string[] = ['Nom','Email','Occupation', 'ActionDetail', 'ActionUpdate', 'ActionDelete'];

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  ngOnInit() {
    this.loadUsers();
  }

  loadUsers(){
    this.userService.getUsers().subscribe((users) => {
      this.UserData = users;
      this.dataSource = new MatTableDataSource<User>(this.UserData);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  onDetail(id: number) {
    this.router.navigate(['user-details/',id]);
  }

  onUpdate(id: number) {
    this.router.navigate(['update-user/',id]);
  }

  onDelete(id: number) {
    this.userService.deleteUser(id).subscribe(() => {
      this.loadUsers();
    });
    console.log('Cliqué sur le bouton Update. ID de l\'utilisateur:', id);
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value.trim().toLowerCase();
    if (this.dataSource) {
      this.dataSource.filter = filterValue;

      if (this.dataSource.paginator) {
        this.dataSource.paginator.firstPage();
      }

      if (this.dataSource.sort) {
        this.dataSource.sort.sort({ id: 'asc', start: 'asc', disableClear: false });
      }
    }
  }

  onAdd() {
    this.router.navigate(['add-user']);
    this.userService.getUsers().subscribe(() => {
      this.loadUsers();
    });
    
  }
  
}
