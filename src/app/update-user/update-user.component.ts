import { Component, OnInit } from '@angular/core';
import { User } from '../models/userData';
import { UserService } from '../user.service';
import { ActivatedRoute,Router } from '@angular/router';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.css']
})
export class UpdateUserComponent implements OnInit{
    
    user: User = { id: 0, name: '', occupation: '', email: '', bio: '' };

    constructor(private userService: UserService, private activatedRoute: ActivatedRoute, private router: Router) {}

    ngOnInit() {
      const userId = parseInt(this.activatedRoute.snapshot.params['id'], 10);

      this.userService.getUserDetails(userId).subscribe(
        (userData: User) => {
          this.user = userData || { id: 0, name: '', occupation: '', email: '', bio: '' };
        },
        (error) => {
          console.error('Erreur lors du chargement des données de l\'utilisateur', error);
        }
      );
    }

    updateUser() {
      this.userService.updateUser(this.user).subscribe(() => {
        console.log('Utilisateur mis à jour avec succès');
      });
    }

}
