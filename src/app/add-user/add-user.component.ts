import { Component } from '@angular/core';
import { UserService } from '../user.service';
import { User } from '../models/userData';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent {
  user: User= { id: 0, name: '', occupation: '', email: '', bio: '' };

  constructor(private userService: UserService){

  }

  addUser(user:User) {
     user.id =  Math.floor(Math.random() * 1000);
     this.userService.addUser(this.user).subscribe(() => {
      console.log('Utilisateur ajouté avec succès');
    });
  }

}
